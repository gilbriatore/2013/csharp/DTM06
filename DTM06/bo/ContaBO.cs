﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using vo;
namespace bo
{
    class ContaBO
    {
        public static Conta depositar(Conta conta, float valor)
        {
            conta.saldo = conta.saldo + valor;
            return conta;
        }

        public static Conta sacar(Conta conta, float valor)
        {
            if (conta.saldo + conta.limite >= valor)
            {
                conta.saldo = conta.saldo - valor;
                return conta;
            }
            else
            {
                return null;
            }
        }
    }
}
