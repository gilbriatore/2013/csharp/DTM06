﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Conta
    {
        public Cliente cliente {set; get;}
        public int agencia {set; get;}
        public int conta {set; get;}
        public float saldo {set; get;}
        public float limite { set; get; }

        public Conta() { }
        public Conta(Cliente cliente, int agencia, int conta, float saldo, float limite)
        {
            this.cliente = cliente;
            this.agencia = agencia;
            this.conta = conta;
            this.saldo = saldo;
            this.limite = limite;
        }
        public Conta(int agencia, int conta)
        {
            this.agencia = agencia;
            this.conta = conta;
        }
    }
}
