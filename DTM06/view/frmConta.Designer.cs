﻿namespace view
{
    partial class frmConta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtLimite = new System.Windows.Forms.TextBox();
            this.lblLimite = new System.Windows.Forms.Label();
            this.txtSaldo = new System.Windows.Forms.TextBox();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.txtConta = new System.Windows.Forms.TextBox();
            this.lblConta = new System.Windows.Forms.Label();
            this.txtAgencia = new System.Windows.Forms.TextBox();
            this.lblAgencia = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnSacar = new System.Windows.Forms.Button();
            this.btnDepositar = new System.Windows.Forms.Button();
            this.grpGrid = new System.Windows.Forms.GroupBox();
            this.grdDados = new System.Windows.Forms.DataGridView();
            this.grdCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdAgencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdConta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdSaldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdLimite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdDisponivel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpDepositos = new System.Windows.Forms.GroupBox();
            this.btnCancelarDeposito = new System.Windows.Forms.Button();
            this.btnOkDeposito = new System.Windows.Forms.Button();
            this.txtValorDeposito = new System.Windows.Forms.TextBox();
            this.lblValorDeposito = new System.Windows.Forms.Label();
            this.cboContaDeposito = new System.Windows.Forms.ComboBox();
            this.lblContaDeposito = new System.Windows.Forms.Label();
            this.cboAgenciaDeposito = new System.Windows.Forms.ComboBox();
            this.lblAgenciaDeposito = new System.Windows.Forms.Label();
            this.grpSaques = new System.Windows.Forms.GroupBox();
            this.btnCancelarSaque = new System.Windows.Forms.Button();
            this.btnOkSaque = new System.Windows.Forms.Button();
            this.txtValorSaque = new System.Windows.Forms.TextBox();
            this.lblvalorSaque = new System.Windows.Forms.Label();
            this.cboContaSaque = new System.Windows.Forms.ComboBox();
            this.lblContaSaque = new System.Windows.Forms.Label();
            this.cboAgenciaSaque = new System.Windows.Forms.ComboBox();
            this.lblAgenciaSaque = new System.Windows.Forms.Label();
            this.grpDados.SuspendLayout();
            this.grpGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDados)).BeginInit();
            this.grpDepositos.SuspendLayout();
            this.grpSaques.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtLimite);
            this.grpDados.Controls.Add(this.lblLimite);
            this.grpDados.Controls.Add(this.txtSaldo);
            this.grpDados.Controls.Add(this.lblSaldo);
            this.grpDados.Controls.Add(this.txtConta);
            this.grpDados.Controls.Add(this.lblConta);
            this.grpDados.Controls.Add(this.txtAgencia);
            this.grpDados.Controls.Add(this.lblAgencia);
            this.grpDados.Controls.Add(this.txtCliente);
            this.grpDados.Controls.Add(this.lblCliente);
            this.grpDados.Location = new System.Drawing.Point(12, 16);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(480, 116);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados";
            // 
            // txtLimite
            // 
            this.txtLimite.Location = new System.Drawing.Point(76, 76);
            this.txtLimite.MaxLength = 10;
            this.txtLimite.Name = "txtLimite";
            this.txtLimite.Size = new System.Drawing.Size(84, 20);
            this.txtLimite.TabIndex = 9;
            // 
            // lblLimite
            // 
            this.lblLimite.Location = new System.Drawing.Point(12, 80);
            this.lblLimite.Name = "lblLimite";
            this.lblLimite.Size = new System.Drawing.Size(100, 23);
            this.lblLimite.TabIndex = 8;
            this.lblLimite.Text = "Limite";
            // 
            // txtSaldo
            // 
            this.txtSaldo.Location = new System.Drawing.Point(380, 48);
            this.txtSaldo.MaxLength = 10;
            this.txtSaldo.Name = "txtSaldo";
            this.txtSaldo.Size = new System.Drawing.Size(84, 20);
            this.txtSaldo.TabIndex = 7;
            // 
            // lblSaldo
            // 
            this.lblSaldo.Location = new System.Drawing.Point(300, 52);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(100, 23);
            this.lblSaldo.TabIndex = 6;
            this.lblSaldo.Text = "Saldo inicial";
            // 
            // txtConta
            // 
            this.txtConta.Location = new System.Drawing.Point(212, 48);
            this.txtConta.MaxLength = 10;
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(84, 20);
            this.txtConta.TabIndex = 5;
            // 
            // lblConta
            // 
            this.lblConta.Location = new System.Drawing.Point(168, 52);
            this.lblConta.Name = "lblConta";
            this.lblConta.Size = new System.Drawing.Size(100, 23);
            this.lblConta.TabIndex = 4;
            this.lblConta.Text = "Conta";
            // 
            // txtAgencia
            // 
            this.txtAgencia.Location = new System.Drawing.Point(76, 48);
            this.txtAgencia.MaxLength = 10;
            this.txtAgencia.Name = "txtAgencia";
            this.txtAgencia.Size = new System.Drawing.Size(84, 20);
            this.txtAgencia.TabIndex = 3;
            // 
            // lblAgencia
            // 
            this.lblAgencia.Location = new System.Drawing.Point(12, 52);
            this.lblAgencia.Name = "lblAgencia";
            this.lblAgencia.Size = new System.Drawing.Size(100, 23);
            this.lblAgencia.TabIndex = 2;
            this.lblAgencia.Text = "Agência";
            // 
            // txtCliente
            // 
            this.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCliente.Location = new System.Drawing.Point(76, 20);
            this.txtCliente.MaxLength = 60;
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(388, 20);
            this.txtCliente.TabIndex = 1;
            // 
            // lblCliente
            // 
            this.lblCliente.Location = new System.Drawing.Point(12, 24);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(100, 23);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.Text = "Cliente";
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(520, 24);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(108, 28);
            this.btnNovo.TabIndex = 1;
            this.btnNovo.Text = "&Nova conta";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnSacar
            // 
            this.btnSacar.Location = new System.Drawing.Point(520, 60);
            this.btnSacar.Name = "btnSacar";
            this.btnSacar.Size = new System.Drawing.Size(108, 28);
            this.btnSacar.TabIndex = 2;
            this.btnSacar.Text = "&Sacar";
            this.btnSacar.UseVisualStyleBackColor = true;
            this.btnSacar.Click += new System.EventHandler(this.btnSacar_Click);
            // 
            // btnDepositar
            // 
            this.btnDepositar.Location = new System.Drawing.Point(520, 100);
            this.btnDepositar.Name = "btnDepositar";
            this.btnDepositar.Size = new System.Drawing.Size(108, 28);
            this.btnDepositar.TabIndex = 3;
            this.btnDepositar.Text = "&Depositar";
            this.btnDepositar.UseVisualStyleBackColor = true;
            this.btnDepositar.Click += new System.EventHandler(this.btnDepositar_Click);
            // 
            // grpGrid
            // 
            this.grpGrid.Controls.Add(this.grdDados);
            this.grpGrid.Location = new System.Drawing.Point(12, 140);
            this.grpGrid.Name = "grpGrid";
            this.grpGrid.Size = new System.Drawing.Size(624, 156);
            this.grpGrid.TabIndex = 4;
            this.grpGrid.TabStop = false;
            this.grpGrid.Text = "groupBox1";
            // 
            // grdDados
            // 
            this.grdDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdCliente,
            this.grdAgencia,
            this.grdConta,
            this.grdSaldo,
            this.grdLimite,
            this.grdDisponivel});
            this.grdDados.Location = new System.Drawing.Point(8, 20);
            this.grdDados.MultiSelect = false;
            this.grdDados.Name = "grdDados";
            this.grdDados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdDados.Size = new System.Drawing.Size(600, 120);
            this.grdDados.TabIndex = 0;
            this.grdDados.TabStop = false;
            // 
            // grdCliente
            // 
            this.grdCliente.HeaderText = "Cliente";
            this.grdCliente.Name = "grdCliente";
            this.grdCliente.ReadOnly = true;
            this.grdCliente.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdCliente.Width = 150;
            // 
            // grdAgencia
            // 
            this.grdAgencia.HeaderText = "Agência";
            this.grdAgencia.Name = "grdAgencia";
            this.grdAgencia.ReadOnly = true;
            this.grdAgencia.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdAgencia.Width = 70;
            // 
            // grdConta
            // 
            this.grdConta.HeaderText = "Conta";
            this.grdConta.Name = "grdConta";
            this.grdConta.ReadOnly = true;
            this.grdConta.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdConta.Width = 70;
            // 
            // grdSaldo
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdSaldo.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdSaldo.HeaderText = "Saldo";
            this.grdSaldo.Name = "grdSaldo";
            this.grdSaldo.ReadOnly = true;
            this.grdSaldo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdSaldo.Width = 80;
            // 
            // grdLimite
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdLimite.DefaultCellStyle = dataGridViewCellStyle5;
            this.grdLimite.HeaderText = "Limite";
            this.grdLimite.Name = "grdLimite";
            this.grdLimite.ReadOnly = true;
            this.grdLimite.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdLimite.Width = 80;
            // 
            // grdDisponivel
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdDisponivel.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdDisponivel.HeaderText = "Disponível";
            this.grdDisponivel.Name = "grdDisponivel";
            this.grdDisponivel.ReadOnly = true;
            this.grdDisponivel.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdDisponivel.Width = 80;
            // 
            // grpDepositos
            // 
            this.grpDepositos.Controls.Add(this.btnCancelarDeposito);
            this.grpDepositos.Controls.Add(this.btnOkDeposito);
            this.grpDepositos.Controls.Add(this.txtValorDeposito);
            this.grpDepositos.Controls.Add(this.lblValorDeposito);
            this.grpDepositos.Controls.Add(this.cboContaDeposito);
            this.grpDepositos.Controls.Add(this.lblContaDeposito);
            this.grpDepositos.Controls.Add(this.cboAgenciaDeposito);
            this.grpDepositos.Controls.Add(this.lblAgenciaDeposito);
            this.grpDepositos.Enabled = false;
            this.grpDepositos.Location = new System.Drawing.Point(16, 304);
            this.grpDepositos.Name = "grpDepositos";
            this.grpDepositos.Size = new System.Drawing.Size(304, 168);
            this.grpDepositos.TabIndex = 5;
            this.grpDepositos.TabStop = false;
            this.grpDepositos.Text = "Depósitos";
            // 
            // btnCancelarDeposito
            // 
            this.btnCancelarDeposito.Location = new System.Drawing.Point(180, 112);
            this.btnCancelarDeposito.Name = "btnCancelarDeposito";
            this.btnCancelarDeposito.Size = new System.Drawing.Size(108, 28);
            this.btnCancelarDeposito.TabIndex = 12;
            this.btnCancelarDeposito.Text = "Cancelar";
            this.btnCancelarDeposito.UseVisualStyleBackColor = true;
            this.btnCancelarDeposito.Click += new System.EventHandler(this.btnCancelarDeposito_Click);
            // 
            // btnOkDeposito
            // 
            this.btnOkDeposito.Location = new System.Drawing.Point(180, 76);
            this.btnOkDeposito.Name = "btnOkDeposito";
            this.btnOkDeposito.Size = new System.Drawing.Size(108, 28);
            this.btnOkDeposito.TabIndex = 11;
            this.btnOkDeposito.Text = "OK";
            this.btnOkDeposito.UseVisualStyleBackColor = true;
            this.btnOkDeposito.Click += new System.EventHandler(this.btnOkDeposito_Click);
            // 
            // txtValorDeposito
            // 
            this.txtValorDeposito.Location = new System.Drawing.Point(56, 44);
            this.txtValorDeposito.MaxLength = 10;
            this.txtValorDeposito.Name = "txtValorDeposito";
            this.txtValorDeposito.Size = new System.Drawing.Size(232, 20);
            this.txtValorDeposito.TabIndex = 10;
            // 
            // lblValorDeposito
            // 
            this.lblValorDeposito.Location = new System.Drawing.Point(8, 48);
            this.lblValorDeposito.Name = "lblValorDeposito";
            this.lblValorDeposito.Size = new System.Drawing.Size(100, 23);
            this.lblValorDeposito.TabIndex = 4;
            this.lblValorDeposito.Text = "Valor";
            // 
            // cboContaDeposito
            // 
            this.cboContaDeposito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboContaDeposito.FormattingEnabled = true;
            this.cboContaDeposito.Location = new System.Drawing.Point(200, 16);
            this.cboContaDeposito.Name = "cboContaDeposito";
            this.cboContaDeposito.Size = new System.Drawing.Size(88, 21);
            this.cboContaDeposito.TabIndex = 3;
            // 
            // lblContaDeposito
            // 
            this.lblContaDeposito.Location = new System.Drawing.Point(152, 20);
            this.lblContaDeposito.Name = "lblContaDeposito";
            this.lblContaDeposito.Size = new System.Drawing.Size(100, 23);
            this.lblContaDeposito.TabIndex = 2;
            this.lblContaDeposito.Text = "Conta";
            // 
            // cboAgenciaDeposito
            // 
            this.cboAgenciaDeposito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAgenciaDeposito.FormattingEnabled = true;
            this.cboAgenciaDeposito.Location = new System.Drawing.Point(56, 16);
            this.cboAgenciaDeposito.Name = "cboAgenciaDeposito";
            this.cboAgenciaDeposito.Size = new System.Drawing.Size(88, 21);
            this.cboAgenciaDeposito.TabIndex = 1;
            // 
            // lblAgenciaDeposito
            // 
            this.lblAgenciaDeposito.Location = new System.Drawing.Point(8, 20);
            this.lblAgenciaDeposito.Name = "lblAgenciaDeposito";
            this.lblAgenciaDeposito.Size = new System.Drawing.Size(100, 23);
            this.lblAgenciaDeposito.TabIndex = 0;
            this.lblAgenciaDeposito.Text = "Agência";
            // 
            // grpSaques
            // 
            this.grpSaques.Controls.Add(this.btnCancelarSaque);
            this.grpSaques.Controls.Add(this.btnOkSaque);
            this.grpSaques.Controls.Add(this.txtValorSaque);
            this.grpSaques.Controls.Add(this.lblvalorSaque);
            this.grpSaques.Controls.Add(this.cboContaSaque);
            this.grpSaques.Controls.Add(this.lblContaSaque);
            this.grpSaques.Controls.Add(this.cboAgenciaSaque);
            this.grpSaques.Controls.Add(this.lblAgenciaSaque);
            this.grpSaques.Enabled = false;
            this.grpSaques.Location = new System.Drawing.Point(332, 304);
            this.grpSaques.Name = "grpSaques";
            this.grpSaques.Size = new System.Drawing.Size(304, 168);
            this.grpSaques.TabIndex = 6;
            this.grpSaques.TabStop = false;
            this.grpSaques.Text = "Depósitos";
            // 
            // btnCancelarSaque
            // 
            this.btnCancelarSaque.Location = new System.Drawing.Point(180, 112);
            this.btnCancelarSaque.Name = "btnCancelarSaque";
            this.btnCancelarSaque.Size = new System.Drawing.Size(108, 28);
            this.btnCancelarSaque.TabIndex = 12;
            this.btnCancelarSaque.Text = "Cancelar";
            this.btnCancelarSaque.UseVisualStyleBackColor = true;
            this.btnCancelarSaque.Click += new System.EventHandler(this.btnCancelarSaque_Click_1);
            // 
            // btnOkSaque
            // 
            this.btnOkSaque.Location = new System.Drawing.Point(180, 76);
            this.btnOkSaque.Name = "btnOkSaque";
            this.btnOkSaque.Size = new System.Drawing.Size(108, 28);
            this.btnOkSaque.TabIndex = 11;
            this.btnOkSaque.Text = "OK";
            this.btnOkSaque.UseVisualStyleBackColor = true;
            this.btnOkSaque.Click += new System.EventHandler(this.btnOkSaque_Click);
            // 
            // txtValorSaque
            // 
            this.txtValorSaque.Location = new System.Drawing.Point(56, 44);
            this.txtValorSaque.MaxLength = 10;
            this.txtValorSaque.Name = "txtValorSaque";
            this.txtValorSaque.Size = new System.Drawing.Size(232, 20);
            this.txtValorSaque.TabIndex = 10;
            // 
            // lblvalorSaque
            // 
            this.lblvalorSaque.Location = new System.Drawing.Point(8, 48);
            this.lblvalorSaque.Name = "lblvalorSaque";
            this.lblvalorSaque.Size = new System.Drawing.Size(100, 23);
            this.lblvalorSaque.TabIndex = 4;
            this.lblvalorSaque.Text = "Valor";
            // 
            // cboContaSaque
            // 
            this.cboContaSaque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboContaSaque.FormattingEnabled = true;
            this.cboContaSaque.Location = new System.Drawing.Point(200, 16);
            this.cboContaSaque.Name = "cboContaSaque";
            this.cboContaSaque.Size = new System.Drawing.Size(88, 21);
            this.cboContaSaque.TabIndex = 3;
            // 
            // lblContaSaque
            // 
            this.lblContaSaque.Location = new System.Drawing.Point(152, 20);
            this.lblContaSaque.Name = "lblContaSaque";
            this.lblContaSaque.Size = new System.Drawing.Size(100, 23);
            this.lblContaSaque.TabIndex = 2;
            this.lblContaSaque.Text = "Conta";
            // 
            // cboAgenciaSaque
            // 
            this.cboAgenciaSaque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAgenciaSaque.FormattingEnabled = true;
            this.cboAgenciaSaque.Location = new System.Drawing.Point(56, 16);
            this.cboAgenciaSaque.Name = "cboAgenciaSaque";
            this.cboAgenciaSaque.Size = new System.Drawing.Size(88, 21);
            this.cboAgenciaSaque.TabIndex = 1;
            // 
            // lblAgenciaSaque
            // 
            this.lblAgenciaSaque.Location = new System.Drawing.Point(8, 20);
            this.lblAgenciaSaque.Name = "lblAgenciaSaque";
            this.lblAgenciaSaque.Size = new System.Drawing.Size(100, 23);
            this.lblAgenciaSaque.TabIndex = 0;
            this.lblAgenciaSaque.Text = "Agência";
            // 
            // frmConta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 488);
            this.Controls.Add(this.grpSaques);
            this.Controls.Add(this.grpDepositos);
            this.Controls.Add(this.grpGrid);
            this.Controls.Add(this.btnDepositar);
            this.Controls.Add(this.btnSacar);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.grpDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conta Corrente";
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.grpGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDados)).EndInit();
            this.grpDepositos.ResumeLayout(false);
            this.grpDepositos.PerformLayout();
            this.grpSaques.ResumeLayout(false);
            this.grpSaques.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.TextBox txtLimite;
        private System.Windows.Forms.Label lblLimite;
        private System.Windows.Forms.TextBox txtSaldo;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.TextBox txtConta;
        private System.Windows.Forms.Label lblConta;
        private System.Windows.Forms.TextBox txtAgencia;
        private System.Windows.Forms.Label lblAgencia;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btnSacar;
        private System.Windows.Forms.Button btnDepositar;
        private System.Windows.Forms.GroupBox grpGrid;
        private System.Windows.Forms.DataGridView grdDados;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdAgencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdConta;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdSaldo;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdLimite;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdDisponivel;
        private System.Windows.Forms.GroupBox grpDepositos;
        private System.Windows.Forms.ComboBox cboContaDeposito;
        private System.Windows.Forms.Label lblContaDeposito;
        private System.Windows.Forms.ComboBox cboAgenciaDeposito;
        private System.Windows.Forms.Label lblAgenciaDeposito;
        private System.Windows.Forms.TextBox txtValorDeposito;
        private System.Windows.Forms.Label lblValorDeposito;
        private System.Windows.Forms.Button btnCancelarDeposito;
        private System.Windows.Forms.Button btnOkDeposito;
        private System.Windows.Forms.GroupBox grpSaques;
        private System.Windows.Forms.Button btnCancelarSaque;
        private System.Windows.Forms.Button btnOkSaque;
        private System.Windows.Forms.TextBox txtValorSaque;
        private System.Windows.Forms.Label lblvalorSaque;
        private System.Windows.Forms.ComboBox cboContaSaque;
        private System.Windows.Forms.Label lblContaSaque;
        private System.Windows.Forms.ComboBox cboAgenciaSaque;
        private System.Windows.Forms.Label lblAgenciaSaque;
    }
}

