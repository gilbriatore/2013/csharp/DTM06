﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using vo;
using bo;
namespace view
{
    public partial class frmConta : Form
    {
        public frmConta()
        {
            InitializeComponent();
        }

        private void desabilitar()
        {
            grpDados.Enabled = false;
            btnNovo.Enabled = false;
            btnDepositar.Enabled = false;
            btnSacar.Enabled = false;
        }

        private void habilitar()
        {
            grpDados.Enabled = true;
            btnNovo.Enabled = true;
            btnDepositar.Enabled = true;
            btnSacar.Enabled = true;
        }

        private void preencherComboSaque()
        {
            int i;
            cboAgenciaSaque.Items.Clear();
            cboContaSaque.Items.Clear();
            for (i = 0; i < grdDados.Rows.Count - 1; i++)
            {
                cboAgenciaSaque.Items.Add(grdDados.Rows[i].Cells["grdAgencia"].Value.ToString());
                cboContaSaque.Items.Add(grdDados.Rows[i].Cells["grdConta"].Value.ToString());
            }
        }

        private void preencherComboDeposito()
        {
            int i;
            cboAgenciaDeposito.Items.Clear();
            cboContaDeposito.Items.Clear();
            for (i = 0; i < grdDados.Rows.Count - 1; i++)
            {
                cboAgenciaDeposito.Items.Add(grdDados.Rows[i].Cells["grdAgencia"].Value.ToString());
                cboContaDeposito.Items.Add(grdDados.Rows[i].Cells["grdConta"].Value.ToString());
            }
        }

        private void atualizarGrid(Conta conta)
        {
            int i;
            float disponivel = conta.saldo + conta.limite;
            for (i = 0; i < grdDados.Rows.Count - 1; i++)
            {
                if (grdDados.Rows[i].Cells["grdAgencia"].Value.ToString() == conta.agencia.ToString() && grdDados.Rows[i].Cells["grdConta"].Value.ToString() == conta.conta.ToString())
                {
                    grdDados.Rows[i].Cells["grdSaldo"].Value = conta.saldo.ToString("N2");
                    grdDados.Rows[i].Cells["grdDisponivel"].Value = disponivel.ToString("N2");  
                }
            }
        }

        private Conta obterConta(Conta conta)
        {
            int i;
            for (i = 0; i < grdDados.Rows.Count - 1; i++)
            {
                if (grdDados.Rows[i].Cells["grdAgencia"].Value.ToString() == conta.agencia.ToString() && grdDados.Rows[i].Cells["grdConta"].Value.ToString() == conta.conta.ToString())
                {
                    Cliente cliente = new Cliente(grdDados.Rows[i].Cells["grdCliente"].Value.ToString());
                    int agencia = int.Parse(grdDados.Rows[i].Cells["grdAgencia"].Value.ToString());
                    int cc = int.Parse(grdDados.Rows[i].Cells["grdConta"].Value.ToString());
                    float saldo = float.Parse(grdDados.Rows[i].Cells["grdSaldo"].Value.ToString());
                    float limite = float.Parse(grdDados.Rows[i].Cells["grdLimite"].Value.ToString());
                    conta.cliente = cliente;
                    conta.agencia = agencia;
                    conta.conta = cc;
                    conta.saldo = saldo;
                    conta.limite = limite;
                    return conta;
                }
            }
            return null;
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            if (txtCliente.Text != "" && txtAgencia.Text != "" && txtConta.Text != "")
            {
                try
                {
                    int agencia = int.Parse(txtAgencia.Text);
                    int conta = int.Parse(txtConta.Text);
                    float saldo = float.Parse(txtSaldo.Text);
                    float limite = float.Parse(txtLimite.Text);
                    Cliente cliente = new Cliente(txtCliente.Text);
                    Conta c = new Conta(cliente, agencia, conta, saldo, limite);
                    if (obterConta(c) == null)
                    {
                        float disponivel = c.saldo + c.limite;
                        grdDados.Rows.Add(c.cliente.nome, c.agencia, c.conta, c.saldo.ToString("N2"), c.limite.ToString("N2"), disponivel.ToString("N2"));
                    }
                    else
                    {
                        MessageBox.Show("Dados ja cadastrados.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show("Erro ao criar conta", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

       private void btnSacar_Click(object sender, EventArgs e)
        {
            desabilitar();
            grpSaques.Enabled = true;
            preencherComboSaque();
            cboAgenciaSaque.Focus();
        }

        private void btnCancelarSaque_Click(object sender, EventArgs e)
        {
            habilitar();
            grpSaques.Enabled = false;
        }

        private void btnOkSaque_Click(object sender, EventArgs e)
        {
            if (cboAgenciaSaque.Text != "" && cboContaSaque.Text != "" && txtValorSaque.Text != "")
            {
                try
                {
                    float valor = float.Parse(txtValorSaque.Text);
                    Conta c = new Conta(int.Parse(cboAgenciaSaque.Text), int.Parse(cboContaSaque.Text));
                    c = obterConta(c);
                    if (c != null)
                    {
                        if (ContaBO.sacar(c, valor) != null)
                        {
                            atualizarGrid(c);
                            btnCancelarSaque_Click(sender, e);
                        }
                        else
                        {
                            MessageBox.Show("Saque não realizado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show("Erro ao sacar", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDepositar_Click(object sender, EventArgs e)
        {
            desabilitar();
            grpDepositos.Enabled = true;
            preencherComboDeposito();
            cboAgenciaSaque.Focus();
        }

        private void btnCancelarDeposito_Click(object sender, EventArgs e)
        {
            habilitar();
            grpDepositos.Enabled = false;
        }

        private void btnOkDeposito_Click(object sender, EventArgs e)
        {
            if (cboAgenciaDeposito.Text != "" && cboContaDeposito.Text != "" && txtValorDeposito.Text != "")
            {
                try
                {
                    float valor = float.Parse(txtValorDeposito.Text);
                    Conta c = new Conta(int.Parse(cboAgenciaDeposito.Text), int.Parse(cboContaDeposito.Text));
                    c = obterConta(c);
                    if (c != null)
                    {
                        ContaBO.depositar(c, valor);
                        atualizarGrid(c);
                        btnCancelarDeposito_Click(sender, e);
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show("Erro ao sacar", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCancelarSaque_Click_1(object sender, EventArgs e)
        {
            habilitar();
            grpSaques.Enabled = false;
        }

    }
}
